package nl.saxion;

import robocode.*;
import robocode.Robot;

import java.awt.*;

public class VietnameseBoi extends Robot {
    boolean movingForward;

    public void run() {

        setColors(Color.pink,Color.pink,Color.pink,Color.pink,Color.pink);
        while(true){
            ahead(100);
            turnGunRight(360);
            back(50);
            turnGunLeft(360);
            turnRight(90);
        }
    }

    public void onScannedRobot(ScannedRobotEvent e){
        if (e.getDistance() < 200){
            fire(5);
        } else {
            fire(1);
        }
    }

    public void onHitBullet(HitByBulletEvent e){
        back(50);
        turnLeft(90);
    }

    public void onHitRobot(HitRobotEvent e){
        back(100);
    }
    public void onHitWall(HitWallEvent e) {
        this.reverseDirection();
    }

    public void reverseDirection() {
        if (this.movingForward) {
            back(100);
            this.movingForward = false;
        } else {
            ahead(100);
            this.movingForward = true;
        }

    }
}
